<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel UI AdminLTE3 Scaffolding
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-ui-adminlte.git`
2. Go inside the folder: `cd laravel-ui-adminlte`
3. Run `cp .env.example .env` then put your database name & credentials.
4. Run `composer install`
5. Run `php artisan key:generate`
6. Run `php artisan migrate`
7. Run `php artisan serve`
8. Open your favorite browser: http://localhost:8000

### Screen shot

Register Page

![Register Page](img/register.png "Register Page")

Login Page

![Login Page](img/login.png "Login Page")

Forgot Password

![Forgot Password](img/forgot.png "Forgot Password")

For more details please visit this link https://github.com/InfyOmLabs/laravel-ui-adminlte
